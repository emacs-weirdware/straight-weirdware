;;;###autoload
(defun straight-recipes-straight-weirdware-retrieve (package)
  "Look up receipe for PACKAGE in Emacs Weirdware.

PACKAGE should be a symbol."
  (when (member (symbol-name package) (straight-recipes-straight-weirdware-list))
    `(,package :type git
               :repo ,(format "https://codeberg.org/emacs-weirdware/%s.git"
                              (symbol-name package)))))

;;;###autoload
(defun straight-recipes-straight-weirdware-list ()
  "Return a list of recipe names available in Emacs Weirdware."
  (list
   "9lc-mode"
   "blight"
   "debase"
   "discomfort"
   "diss"
   "dnt"
   "exwm-firefox"
   "exwm-mff"
   "exwm-ss"
   "geoclue"
   "hyperspace"
   "lemon"
   "nominatim"
   "nssh"
   "org-street"
   "scratch"
   "shell-here"
   "tl1-mode"
   ))

;;;###autoload
(defun straight-recipes-straight-weirdware-version ()
  "Return the current version of the Emacs Weirdware retriever."
  1)
